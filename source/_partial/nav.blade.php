<nav class="navbar navbar-toggleable-md navbar-inverse bg-inverse">
  <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarsExampleDefault" aria-controls="navbar" aria-expanded="false" aria-label="Переключить навигацию">
    <span class="navbar-toggler-icon"></span>
  </button>
  <a class="navbar-brand" href="{!! $page->url('/') !!}">{{ $page->sitetitle }}</a>
  <div class="collapse navbar-collapse" id="navbar">
    <ul class="navbar-nav mr-auto">
      {!! $page->navitem('', 'Обложка') !!}
      {!! $page->navitem('/toc', 'Список игр') !!}
      {!! $page->navitem('/rules', 'Правила') !!}
      {!! $page->navitem('/add', 'Добавить игру') !!}
    </ul>
  </div>
</nav>
