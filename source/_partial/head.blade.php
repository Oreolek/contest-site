  <head>
  	<meta charset="utf-8">
    <title>{{ $page->title }}</title>
    <meta http-equiv="x-ua-compatible" content="ie=edge">
  	<meta name="viewport" content="width=device-width, initial-scale=1">
  	<meta name="generator" content="Jigsaw">
    <meta name="description" content="{{ $page->description }}">
    {!! $page->css('/css/style.css') !!}
  </head>
