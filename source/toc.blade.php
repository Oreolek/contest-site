---
threed: true
---
@extends('_layouts.master')

@section('contents')
<div class="toc">       
  @foreach ($posts as $post)
    <a href="{!! $post->getPath() !!}">
    {!! $page->thumbnail(
      $post->feature,
      $post->title,
      $page->firstwords($post->content))
    !!}
    </a>
  @endforeach
</div>
@endsection
