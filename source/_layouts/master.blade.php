<!doctype html>
<html>
  @include('_partial.head')

  <body>

    @include('_partial.nav')

    <div class="container">
      <div class="row">
        <div class="col-12">
          <main>
            <h1>{{ $page->title }}</h1>

            @yield('contents')
          </main>
       </div>
      </div>
    </div>

    @include('_partial.footer')
  </body>
</html>
