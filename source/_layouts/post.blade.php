@extends('_layouts.master')

@section('contents')
<section class="main">
  <div class="post-header">
	  <div class="post-headerimage">
      {!! $page->img($page->feature, $page->title) !!}
	  </div>
		<div class="post-headertext">
      <h2>{{ $page->title }}</h2>
    </div>
  </div>
	<div class="row">
		<div class="col-12">
      @yield('content')
    </div>
  </div>

  @if ($page->getPrevious())
    <p>Read the previous post:
        <a href="{{ $page->getPrevious()->path }}">{{ $page->getPrevious()->title }}</a>
    </p>
  @endif
  @if ($page->getNext())
    <p>Read the next post:
        <a href="{{ $page->getNext()->path }}">{{ $page->getNext()->title }}</a>
    </p>
  @endif
</section>
@endsection
