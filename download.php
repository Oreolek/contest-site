<?php
function download_file($url, $out) {
  set_time_limit(0);
  $fp = fopen ($out, 'w+');
  $ch = curl_init(str_replace(" ","%20",$url));
  curl_setopt($ch, CURLOPT_TIMEOUT, 50);
  // write curl response to file
  curl_setopt($ch, CURLOPT_FILE, $fp); 
  curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
  // get curl response
  curl_exec($ch); 
  curl_close($ch);
  fclose($fp);
}
