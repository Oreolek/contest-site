<?php
require_once "vendor/autoload.php";
require_once "download.php";
use Cocur\Slugify\Slugify;
use \Intervention\Image\ImageManagerStatic as Image;

$config = parse_ini_file("config.ini");

$client = \Directus\SDK\ClientFactory::create($config['DIRECTUS_TOKEN'], [
  'base_url' => $config['DIRECTUS_URL'],
]);

try {
  $posts = $client->getItems($config['DIRECTUS_TABLE']);
  if (!file_exists('source/_games')) {
    mkdir('source/_games');
  }
  $slugify = new Slugify();
  $tidy = new tidy();
  foreach($posts as $post) {
    if ($post->active !== 1) { // skip drafts or deleted
      continue;
    }
    $filename = 'source/_games/'.$slugify->slugify($post->name).'.md';
    $content = str_replace("\r", "", $post->content);
    $content = $tidy->repairString(trim($content), [
      'show-body-only' => true
    ]);
    $page = '---'.PHP_EOL.
      'extends: _layouts.post'.PHP_EOL.
      'title: '.$post->name.PHP_EOL.
      'date: '.$post->created_at.PHP_EOL.
      'feature: _'.$post->cover->name.PHP_EOL.
      '---'.PHP_EOL.
      $content;
    download_file(
      $config['DIRECTUS_URL'].'/'.$config['DIRECTUS_STORAGE'].'/'.$post->cover->name,
      'source/_games/_'.$post->cover->name
    );
    $file = fopen($filename, 'w');
    fwrite($file, $page);
    fclose($file);
  }
} catch (Exception $e) {
  echo $e->getMessage();
}

return [
  'baseUrl' => '/',
  'production' => true,
  'collections' => [
    'games' => [
      'path' => 'game/{filename}',
      'sort' => '-date'
    ]
  ],
  'title' => $config['SITE_TITLE'], // site title
  'sitetitle' => $config['SITE_TITLE'], // site title
  'description' => "",

  /**
   * Helpers start here
   */

  'normalizeUrl' => function($page, $target) {
    if ($target === '') {
      return '/';
    }
    if ($target[0] === '/') {
      $target = ltrim($target, '/');
    }
    if (strpos($target, 'http:') === FALSE && strpos($target, 'https:') === FALSE) {
      $target = $page->baseUrl.$target;
    }
    return $target;
  },
  'css' => function($page, $target){
    return '<link rel="stylesheet" href="'.$page->normalizeUrl($target).'" />';
  },
  'js' => function($page, $target) {
    return '<script src="'.$page->normalizeUrl($target).'"></script>';
  },
  'prevnext' => function($page, $previous = '', $next = '', $classname = '') {
    $out = '<ul class="nav '.$classname.'">';
    if (!empty($previous)) {
      $out .= '<li><a href="'.$page->url($previous).'"><strong>&uarr;PREVIOUS</strong></a></li>';
      if (!empty($next)) {
        $out .= '<li>&nbsp;&mdash;&nbsp;</li>';
      }
    }
    if (!empty($next)) {
      $out .= '<li><a href="'.$page->url($next).'"><strong>NEXT&darr;</strong></a></li>';
    }
    $out .= '</ul>';
    return $out;
  },
  'thumbnail' => function($page, $image, $title, $subtitle) {
    $slugify = new Slugify();
    $imagefile = __DIR__ . '/source/_posts/' . $date . '/' . $image;
    $outdir = __DIR__ . '/build_local/blog/' . $date . '/' . $slugify->slugify($title);
    if (!file_exists($outdir)) {
      mkdir($outdir, 0755, true);
    }
    $imagep = Image::make($imagefile);
    if (!file_exists($outdir.'/_small'.$image)) {
      $imagep->resize(300, null, function ($constraint) {
         $constraint->aspectRatio();
       })->save($outdir.'/thumb'.$image);
    }
    return '<div class="thumbnail">'.
      $page->img($image, $title).
      '<p><strong>'.$title.'</strong><br>'.$subtitle.'</p>'.
      '</div>';
  },
  'imgurl' => function($page, $url) {
    return $page->baseUrl.$config['IMAGE_DIR']."/".ltrim($url, '/');
  },
  'img' => function($page, $url, $title) {
    return '<img src="'.$page->imgurl($url).'" alt="'.$title.'">';
  },
  'url' => function($page, $url) {
    return $page->normalizeUrl($url);
  },
  'firstwords' => function($page, $text, $limit = 20) {
    $text = preg_replace('/\s+/', ' ', trim($text));
    $words = explode(" ", $text); // an array

    // if number of words you want to get is greater than number of words in the string
    if ($limit > count($words)) {
      // then use number of words in the string
      $limit = count($words);
    }

    $new_string = "";
    for ($i = 0; $i < $limit; $i++) {
      $new_string .= $words[$i] . " ";
    }

    $tidy = new tidy();

    return $tidy->repairString(trim($new_string), [
      'show-body-only' => true
    ]);
  },
  'navitem' => function($page, $url, $title) {
    $class = '';
    $sr = '';
    if ($page->getPath() === $url) {
      $class=' active';
      $sr = ' <span class="sr-only">(текущая)</span>';
    }
    return '<li class="nav-item'.$class.'">'.
      '<a class="nav-link" href="'.$page->url($url).'">'.$title.$sr.'</a></li>';
  }
];
